public class Knapsack {
    private Node[] arrayOfElements;
    private int capacity;
    private int maxWeight = 0;
    private int maxCost = 0;


    Knapsack(int[] arrayOfWeights, int[] arrayOfCosts, int capacity) {
        setCapacity(capacity);
        arrayOfElements = new Node[arrayOfWeights.length];
        for (int i = 0; i < arrayOfWeights.length; i++) {
            arrayOfElements[i] = new Node(arrayOfWeights[i], arrayOfCosts[i]);
        }
    }


    public int getMaxCostUsingBruteForce() {
        setMaxCost(0);
        setMaxWeight(0);

        for (int i = 0; i < Math.pow(2, arrayOfElements.length); i++) {
            String bitString = Integer.toBinaryString(i);
            int currentWeight = 0;
            int currentCost = 0;
            for (int j = 0; j < bitString.length(); j++) {
                if (bitString.charAt(j) == '1') {
                    currentWeight += arrayOfElements[j].getWeight();
                    currentCost += arrayOfElements[j].getCost();
                }
            }
            if (currentWeight <= getCapacity() && currentCost >= getMaxCost()) {
                setMaxWeight(currentWeight);
                setMaxCost(currentCost);
            }
        }

        return getMaxCost();
    }


    public int getMaxCostUsingGreedyAlgorithm() {
        Node[] backupOfArray = arrayOfElements.clone();

        setMaxCost(0);
        setMaxWeight(0);

        int maxWeight = 0;
        int maxCost = 0;

        Node[] sortedArrayOfElements = new Node[arrayOfElements.length];
        for (int i = 0; i < arrayOfElements.length - 1; i++) {
            int indexOfMaxUnitCost = i;
            float maxUnitCost = (float)arrayOfElements[indexOfMaxUnitCost].getCost()/arrayOfElements[indexOfMaxUnitCost].getWeight();
            for (int j = i + 1; j < sortedArrayOfElements.length; j++) {
                float currentUnitCost = (float)arrayOfElements[j].getCost()/arrayOfElements[j].getWeight();
                if (currentUnitCost > maxUnitCost) {
                    maxUnitCost = currentUnitCost;
                    indexOfMaxUnitCost = j;
                }
            }
            sortedArrayOfElements[i] = arrayOfElements[indexOfMaxUnitCost];
            arrayOfElements[indexOfMaxUnitCost] = arrayOfElements[i];
        }
        sortedArrayOfElements[sortedArrayOfElements.length - 1] = arrayOfElements[arrayOfElements.length - 1];

        for (int i = 0; i < sortedArrayOfElements.length; i++) {
            maxWeight += sortedArrayOfElements[i].getWeight();
            maxCost += sortedArrayOfElements[i].getCost();
            if (maxWeight > getCapacity()) {
                maxWeight -= sortedArrayOfElements[i].getWeight();
                maxCost -= sortedArrayOfElements[i].getCost();
                break;
            }
        }

        setMaxWeight(maxWeight);
        setMaxCost(maxCost);

        arrayOfElements = backupOfArray;

        return getMaxCost();
    }


    public int getMaxCostUsingDynamicProgramming() {
        int[][] matrix = new int[getCapacity() + 1][arrayOfElements.length + 1];

        for (int j = 1; j <= arrayOfElements.length; j++) {
            for (int w = 1; w <= getCapacity(); w++) {
                if (arrayOfElements[j - 1].getWeight() <= w) {
                    matrix[w][j] = Math.max(matrix[w][j - 1], matrix[w - arrayOfElements[j - 1].getWeight()][j - 1] + arrayOfElements[j - 1].getCost());
                } else {
                    matrix[w][j] = matrix[w][j - 1];
                }
            }
        }

        setMaxCost(matrix[getCapacity()][arrayOfElements.length]);

        return getMaxCost();
    }

    public int test() {
        int[][] matrix = new int[1001][11];
        int[] weights = {83, 39, 136, 111, 99, 107, 21, 59, 44, 32};
        int[] costs = {107, 114, 109, 118, 96, 32, 150, 144, 86, 115};

        for (int j = 1; j <= 10; j++) {
            for (int w = 1; w <= 1000; w++) {
                if (weights[j - 1] <= w) {
                    matrix[w][j] = Math.max(matrix[w][j - 1], matrix[w - weights[j - 1]][j - 1] + costs[j - 1]);
                } else {
                    matrix[w][j] = matrix[w][j - 1];
                }
            }
        }
        System.out.println(matrix[1000][10]);

        return 0;
    }


    public int getCapacity() { return capacity; }
    public int getMaxWeight() { return maxWeight; }
    public int getMaxCost() { return maxCost; }


    public void setCapacity(int capacity) { this.capacity = capacity;  }
    private void setMaxWeight(int maxWeight) { this.maxWeight = maxWeight; }
    private void setMaxCost(int maxCost) { this.maxCost = maxCost; }
}
