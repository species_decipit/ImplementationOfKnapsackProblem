import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new FileReader("input.txt"));

        int size = sc.nextInt();

        int[] arrayOfWeights = new int[size];
        int[] arrayOfCosts = new int[size];
        int capacity, maxCost;
        long start, end;
        Knapsack myKnapsack;

        for (int i = 0; i < size; i++) { arrayOfWeights[i] = sc.nextInt(); }
        for (int i = 0; i < size; i++) { arrayOfCosts[i] = sc.nextInt(); }
        capacity = sc.nextInt();

        myKnapsack = new Knapsack(arrayOfWeights, arrayOfCosts, capacity);

        start = System.nanoTime();
        myKnapsack.getMaxCostUsingBruteForce();
        end = System.nanoTime();
        System.out.println("Max cost (using Brute Force implementation): " + myKnapsack.getMaxCost());
        System.out.println("Work time of Brute Force implementation (in seconds) : " + ((end - start)/Math.pow(10, 9)) + "\n");

        start = System.nanoTime();
        myKnapsack.getMaxCostUsingGreedyAlgorithm();
        end = System.nanoTime();
        System.out.println("Max cost (using Greedy Algorithm): " + myKnapsack.getMaxCost());
        System.out.println("Work time of Greedy Algorithm (in seconds)" + ((end - start)/Math.pow(10, 9)) + "\n");

        start = System.nanoTime();
        myKnapsack.getMaxCostUsingDynamicProgramming();
        end = System.nanoTime();
        System.out.println("Max cost (using Dynamic Programming): " + myKnapsack.getMaxCost());
        System.out.println("Work time of Dynamic Programming approach (in seconds): " + ((end - start)/Math.pow(10, 9)) + "\n");

        sc.close();
    }
}
