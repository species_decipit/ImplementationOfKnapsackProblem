public class Node {
    private int weight;
    private int cost;

    Node(int weight, int cost) {
        setWeight(weight);
        setCost(cost);
    }

    public void setWeight(int weight) { this.weight = weight; }
    public void setCost(int cost) { this.cost = cost; }

    public int getWeight() { return weight; }
    public int getCost() { return cost; }
}
